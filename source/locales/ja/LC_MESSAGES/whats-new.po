# Japanese translations for Debian release notes
# Debian リリースノートの日本語訳
# Noritada Kobayashi <nori1@dolphin.c.u-tokyo.ac.jp>, 2006.
# Hisashi Morita, 2009.
# Hideki Yamane <henrich@debian.org>, 2010-2023
# hoxp18 <hoxp18@noramail.jp>, 2019.
# This file is distributed under the same license as Debian release notes.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 12\n"
"Report-Msgid-Bugs-To: debian-doc@lists.debian.org\n"
"POT-Creation-Date: 2024-10-27 12:46+0100\n"
"PO-Revision-Date: 2023-07-04 19:47+0900\n"
"Last-Translator: Hideki Yamane <henrich@debian.org>\n"
"Language: ja\n"
"Language-Team: Japanese <debian-doc@debian.or.jp>\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.3\n"

#: ../whats-new.rst:4
msgid "What's new in Debian |RELEASE|"
msgstr "Debian |RELEASE| の最新情報"

#: ../whats-new.rst:6
msgid ""
"The `Wiki <https://wiki.debian.org/NewInBookworm>`__ has more information"
" about this topic."
msgstr "この章のより詳しい情報は `Wiki <https://wiki.debian.org/NewInBookworm>`__ を参照してください。"

#: ../whats-new.rst:12
msgid "Supported architectures"
msgstr "サポートするアーキテクチャ"

#: ../whats-new.rst:14
msgid ""
"The following are the officially supported architectures for Debian "
"|RELEASE|:"
msgstr "Debian |RELEASENAME| で公式にサポートされているアーキテクチャは以下のとおりです。"

#: ../whats-new.rst:17
msgid "32-bit PC (``i386``) and 64-bit PC (``amd64``)"
msgstr ""

#: ../whats-new.rst:19
msgid "64-bit ARM (``arm64``)"
msgstr ""

#: ../whats-new.rst:21
msgid "ARM EABI (``armel``)"
msgstr ""

#: ../whats-new.rst:23
msgid "ARMv7 (EABI hard-float ABI, ``armhf``)"
msgstr ""

#: ../whats-new.rst:25
msgid "little-endian MIPS (``mipsel``)"
msgstr ""

#: ../whats-new.rst:27
msgid "64-bit little-endian MIPS (``mips64el``)"
msgstr ""

#: ../whats-new.rst:29
msgid "64-bit little-endian PowerPC (``ppc64el``)"
msgstr ""

#: ../whats-new.rst:31
msgid "IBM System z (``s390x``)"
msgstr ""

#: ../whats-new.rst:36
#, fuzzy
msgid "Baseline bump for 32-bit PC to i686"
msgstr "32-bit PC の最低ラインが i686 に引き上げられました"

#: ../whats-new.rst:34
#, fuzzy
msgid ""
"The 32-bit PC support (known as the Debian architecture i386) now "
"requires the \"long NOP\" instruction. Please refer to "
":ref:`i386-is-i686` for more information."
msgstr ""
"(Debian での i386 アーキテクチャとして知られる) 32-bit PC をサポートするには \"long NOP\" "
"命令を含むことが必要となりました。詳細については :ref:`i386-is-i686` を参照してください。"

#: ../whats-new.rst:38
#, fuzzy
msgid ""
"You can read more about port status, and port-specific information for "
"your architecture at the `Debian port web pages "
"<https://www.debian.org/ports/>`__."
msgstr ""
"移植状況の詳細や、お使いの移植版に特有の情報については、`Debian "
"の移植版に関するウェブページ<https://www.debian.org/ports/>`__で読むことができます。"

#: ../whats-new.rst:44
msgid "Archive areas"
msgstr "アーカイブエリア"

#: ../whats-new.rst:46
msgid ""
"The following archive areas, mentioned in the Social Contract and in the "
"Debian Policy, have been around for a long time:"
msgstr "以下の、社会契約と Debian ポリシーで言及されているアーカイブエリアが長期間に渡って存在しています:"

#: ../whats-new.rst:49
msgid "main: the Debian distribution;"
msgstr "main: Debian ディストリビューションそのもの"

#: ../whats-new.rst:51
msgid ""
"contrib: supplemental packages intended to work with the Debian "
"distribution, but which require software outside of the distribution to "
"either build or function;"
msgstr ""
"contrib: Debian "
"ディストリビューションと互換性がある補助パッケージ群だが、ビルドまたは動作にディストリビューション外のソフトウェアを必要とする"

#: ../whats-new.rst:55
msgid ""
"non-free: supplemental packages intended to work with the Debian "
"distribution that do not comply with the DFSG or have other problems that"
" make their distribution problematic."
msgstr ""
"non-free: DFSG (Debian フリーソフトウェアガイドライン) "
"に適合しない、あるいは配布を難しくする他の問題があるが、Debian ディストリビューションと互換性がある補助パッケージ群"

#: ../whats-new.rst:59
msgid ""
"Following the `2022 General Resolution about non-free firmware "
"<https://www.debian.org/vote/2022/vote_003>`__, the 5th point of the "
"Social Contract was extended with the following sentence:"
msgstr ""
"`non-free なファームウェアに関する2022年の一般決議 "
"<https://www.debian.org/vote/2022/vote_003>`__ "
"によって、社会契約の第5条へ以下の文章が追加されました:"

#: ../whats-new.rst:63
msgid ""
"The Debian official media may include firmware that is otherwise not part"
" of the Debian system to enable use of Debian with hardware that requires"
" such firmware."
msgstr ""
"Debian の公式メディアには、Debian "
"システムの一部ではないファームウェアが含まれていることがあります。これはそのようなファームウェアを必要とするハードウェアで Debian "
"を利用可能とするのに他の方法がないためです。"

#: ../whats-new.rst:67
msgid ""
"While it's not mentioned explicitly in either the Social Contract or "
"Debian Policy yet, a new archive area was introduced, making it possible "
"to separate non-free firmware from the other non-free packages:"
msgstr ""
"未だ社会契約や Debian ポリシーには明示的に反映されていませんが、新しいアーカイブエリアが導入されたことで、non-free "
"なファームウェアを他の non-free なパッケージから隔離できています。"

#: ../whats-new.rst:71
msgid "non-free-firmware"
msgstr "non-free-firmware"

#: ../whats-new.rst:73
msgid ""
"Most non-free firmware packages have been moved from ``non-free`` to "
"``non-free-firmware`` in preparation for the Debian |RELEASE| release. "
"This clean separation makes it possible to build official installation "
"images with packages from ``main`` and from ``non-free-firmware``, "
"without ``contrib`` or ``non-free``. In turn, these installation images "
"make it possible to install systems with only ``main`` and ``non-free-"
"firmware``, without ``contrib`` or ``non-free``."
msgstr ""
"ほとんどの non-free なファームウェアパッケージが Debian |RELEASE| リリースの準備中に ``non-free`` から "
"``non-free-firmware`` へ移動されました。このクリーンな分離によって ``contrib`` や ``non-free`` "
"を利用しない、``main`` および ``non-free-firmware`` "
"のパッケージを利用した公式インストールイメージのビルドが可能になりました。同様に、これらのインストールイメージで ``contrib`` や "
"``non-free`` 無しで ``main`` および ``non-free-firmware`` "
"のみでシステムをインストール可能となっています。"

#: ../whats-new.rst:81
msgid "See :ref:`non-free-firmware` for upgrades from |OLDRELEASENAME|."
msgstr "|OLDRELEASENAME| からのアップグレードについては :ref:`non-free-firmware` を参照してください。"

#: ../whats-new.rst:86
msgid "What's new in the distribution?"
msgstr "ディストリビューションの最新情報"

#: ../whats-new.rst:91
msgid ""
"This new release of Debian again comes with a lot more software than its "
"predecessor |OLDRELEASENAME|; the distribution includes over |PACKAGES-"
"NEW| new packages, for a total of over |PACKAGES-TOTAL| packages. Most of"
" the software in the distribution has been updated: over |PACKAGES-"
"UPDATED| software packages (this is |PACKAGES-UPDATE-PERCENT| of all "
"packages in |OLDRELEASENAME|). Also, a significant number of packages "
"(over |PACKAGES-REMOVED|, |PACKAGES-REMOVED-PERCENT| of the packages in "
"|OLDRELEASENAME|) have for various reasons been removed from the "
"distribution. You will not see any updates for these packages and they "
"will be marked as \"obsolete\" in package management front-ends; see "
":ref:`obsolete`."
msgstr ""
"Debian のこの新しいリリースには、一つ前のリリースである |OLDRELEASENAME| "
"に含まれていたよりさらに多くのソフトウェアが含まれています。このディストリビューションには、|PACKAGES-NEW| "
"以上の新しいパッケージが含まれており、全体のパッケージ数は |PACKAGES-TOTAL| "
"以上になりました。ディストリビューション中のほとんどのソフトウェア、すなわち約 |PACKAGES-UPDATED| ものソフトウェアパッケージ "
"(これは |OLDRELEASENAME| のパッケージ全体の |PACKAGES-UPDATE-PERCENT| にあたります) "
"が更新されました。また、かなりの数のパッケージ (|OLDRELEASENAME| のパッケージの |PACKAGES-REMOVED-"
"PERCENT| にあたる |PACKAGES-REMOVED| 以上) "
"が、様々な理由でディストリビューションから取り除かれました。これらのパッケージは更新されることはなく、パッケージ管理用のフロントエンドでは "
"'obsolete' というマークが付けられます。これについては :ref:`obsolete` を参照してください。"

#: ../whats-new.rst:106
msgid "Desktops and well known packages"
msgstr "デスクトップとよく知られているパッケージ"

#: ../whats-new.rst:108
msgid ""
"Debian again ships with several desktop applications and environments. "
"Among others it now includes the desktop environments GNOME 43, KDE "
"Plasma 5.27, LXDE 11, LXQt 1.2.0, MATE 1.26, and Xfce 4.18."
msgstr ""
"Debian は今回も複数のデスクトップアプリケーションとデスクトップ環境をサポートしています。中でも GNOME 43, KDE Plasma "
"5.27, LXDE 11, LXQt 1.2.0, MATE 1.26, Xfce 4.18 があります。"

#: ../whats-new.rst:112
msgid ""
"Productivity applications have also been upgraded, including the office "
"suites:"
msgstr "事務用アプリケーションもオフィススイートを含めてアップグレードされています:"

#: ../whats-new.rst:115
msgid "LibreOffice is upgraded to version 7.4;"
msgstr "LibreOffice が 7.4 へアップグレードされました。"

#: ../whats-new.rst:117
msgid "GNUcash is upgraded to 4.13;"
msgstr "GNUcash が 4.13 へアップグレードされました。"

#: ../whats-new.rst:119
msgid ""
"Among many others, this release also includes the following software "
"updates:"
msgstr "またこのリリースには、特に挙げるなら、以下のソフトウェアの更新も含まれています:"

#: ../whats-new.rst:123
msgid "Package"
msgstr "パッケージ"

#: ../whats-new.rst:123
msgid "Version in |OLDRELEASE| (|OLDRELEASENAME|)"
msgstr "|OLDRELEASE| (|OLDRELEASENAME|) でのバージョン"

#: ../whats-new.rst:123
msgid "Version in |RELEASE| (|RELEASENAME|)"
msgstr "|RELEASE| (|RELEASENAME|) でのバージョン"

#: ../whats-new.rst:127
msgid "Apache"
msgstr ""

#: ../whats-new.rst:127
msgid "2.4.54"
msgstr "2.4.54"

#: ../whats-new.rst:127 ../whats-new.rst:163
msgid "2.4.57"
msgstr "2.4.57"

#: ../whats-new.rst:129
msgid "Bash"
msgstr ""

#: ../whats-new.rst:129
msgid "5.1"
msgstr "5.1"

#: ../whats-new.rst:129
msgid "5.2.15"
msgstr "5.2.15"

#: ../whats-new.rst:131
msgid "BIND DNS Server"
msgstr ""

#: ../whats-new.rst:131
msgid "9.16"
msgstr "9.16"

#: ../whats-new.rst:131
msgid "9.18"
msgstr "9.18"

#: ../whats-new.rst:133
msgid "Cryptsetup"
msgstr ""

#: ../whats-new.rst:133
msgid "2.3"
msgstr "2.3"

#: ../whats-new.rst:133
msgid "2.6"
msgstr "2.6"

#: ../whats-new.rst:135
msgid "Emacs"
msgstr "Emacs"

#: ../whats-new.rst:135
msgid "27.1"
msgstr "27.1"

#: ../whats-new.rst:135
msgid "28.2"
msgstr "28.2"

#: ../whats-new.rst:137
msgid "Exim default e-mail server"
msgstr ""

#: ../whats-new.rst:137
msgid "4.94"
msgstr "4.94"

#: ../whats-new.rst:137
msgid "4.96"
msgstr "4.96"

#: ../whats-new.rst:140
msgid "GNU Compiler Collection as default compiler"
msgstr ""

#: ../whats-new.rst:140
msgid "10.2"
msgstr "10.2"

#: ../whats-new.rst:140
msgid "12.2"
msgstr "12.2"

#: ../whats-new.rst:144
msgid "GIMP"
msgstr ""

#: ../whats-new.rst:144
msgid "2.10.22"
msgstr "2.10.22"

#: ../whats-new.rst:144
msgid "2.10.34"
msgstr "2.10.34"

#: ../whats-new.rst:146
msgid "GnuPG"
msgstr ""

#: ../whats-new.rst:146
msgid "2.2.27"
msgstr "2.2.27"

#: ../whats-new.rst:146
msgid "2.2.40"
msgstr "2.2.40"

#: ../whats-new.rst:148
msgid "Inkscape"
msgstr ""

#: ../whats-new.rst:148
msgid "1.0.2"
msgstr "1.0.2"

#: ../whats-new.rst:148
msgid "1.2.2"
msgstr "1.2.2"

#: ../whats-new.rst:150
msgid "the GNU C library"
msgstr ""

#: ../whats-new.rst:150
msgid "2.31"
msgstr "2.31"

#: ../whats-new.rst:150
msgid "2.36"
msgstr "2.36"

#: ../whats-new.rst:152
msgid "Linux kernel image"
msgstr "Linux カーネルイメージ"

#: ../whats-new.rst:152
msgid "5.10 series"
msgstr "5.10 シリーズ"

#: ../whats-new.rst:152
msgid "6.1 series"
msgstr "6.1 シリーズ"

#: ../whats-new.rst:154
msgid "LLVM/Clang toolchain"
msgstr "LLVM/Clang ツールチェイン"

#: ../whats-new.rst:154
msgid "9.0.1 and 11.0.1 (default) and 13.0.1"
msgstr "9.0.1, 11.0.1 (デフォルト) そして 13.0.1"

#: ../whats-new.rst:154
msgid "13.0.1 and 14.0 (default) and 15.0.6"
msgstr "13.0.1, 14.0 (デフォルト) そして 15.0.6"

#: ../whats-new.rst:157
msgid "MariaDB"
msgstr ""

#: ../whats-new.rst:157
msgid "10.5"
msgstr "10.5"

#: ../whats-new.rst:157
msgid "10.11"
msgstr "10.11"

#: ../whats-new.rst:159
msgid "Nginx"
msgstr ""

#: ../whats-new.rst:159
msgid "1.18"
msgstr "1.18"

#: ../whats-new.rst:159
msgid "1.22"
msgstr "1.22"

#: ../whats-new.rst:161
msgid "OpenJDK"
msgstr ""

#: ../whats-new.rst:161
msgid "11"
msgstr "11"

#: ../whats-new.rst:161
msgid "17"
msgstr "17"

#: ../whats-new.rst:163
msgid "OpenLDAP"
msgstr "OpenLDAP"

#: ../whats-new.rst:163
msgid "2.5.13"
msgstr "2.5.13"

#: ../whats-new.rst:165
msgid "OpenSSH"
msgstr ""

#: ../whats-new.rst:165
msgid "8.4p1"
msgstr "8.4p1"

#: ../whats-new.rst:165
msgid "9.2p1"
msgstr "9.2p1"

#: ../whats-new.rst:167
msgid "OpenSSL"
msgstr ""

#: ../whats-new.rst:167
msgid "1.1.1n"
msgstr "1.1.1n"

#: ../whats-new.rst:167
msgid "3.0.8"
msgstr "3.0.8"

#: ../whats-new.rst:169
msgid "Perl"
msgstr ""

#: ../whats-new.rst:169
msgid "5.32"
msgstr "5.32"

#: ../whats-new.rst:169
msgid "5.36"
msgstr "5.36"

#: ../whats-new.rst:171
msgid "PHP"
msgstr ""

#: ../whats-new.rst:171
msgid "7.4"
msgstr "7.4"

#: ../whats-new.rst:171 ../whats-new.rst:185
msgid "8.2"
msgstr "8.2"

#: ../whats-new.rst:173
msgid "Postfix MTA"
msgstr ""

#: ../whats-new.rst:173
msgid "3.5"
msgstr "3.5"

#: ../whats-new.rst:173
msgid "3.7"
msgstr "3.7"

#: ../whats-new.rst:175
msgid "PostgreSQL"
msgstr ""

#: ../whats-new.rst:175
msgid "13"
msgstr "13"

#: ../whats-new.rst:175
msgid "15"
msgstr "15"

#: ../whats-new.rst:177
msgid "Python 3"
msgstr "Python 3"

#: ../whats-new.rst:177
msgid "3.9.2"
msgstr "3.9.2"

#: ../whats-new.rst:177
msgid "3.11.2"
msgstr "3.11.2"

#: ../whats-new.rst:179
msgid "Rustc"
msgstr "Rustc"

#: ../whats-new.rst:179
msgid "1.48"
msgstr "1.48"

#: ../whats-new.rst:179
msgid "1.63"
msgstr "1.63"

#: ../whats-new.rst:181
msgid "Samba"
msgstr "Samba"

#: ../whats-new.rst:181
msgid "4.13"
msgstr "4.13"

#: ../whats-new.rst:181
msgid "4.17"
msgstr "4.17"

#: ../whats-new.rst:183
msgid "Systemd"
msgstr ""

#: ../whats-new.rst:183
msgid "247"
msgstr "247"

#: ../whats-new.rst:183
msgid "252"
msgstr "252"

#: ../whats-new.rst:185
msgid "Vim"
msgstr "Vim"

#: ../whats-new.rst:185
msgid "9.0"
msgstr "9.0"

#: ../whats-new.rst:191
msgid "More translated man pages"
msgstr "さらに翻訳された manpage"

#: ../whats-new.rst:193
msgid ""
"Thanks to our translators, more documentation in ``man``-page format is "
"available in more languages than ever. For example, many man pages are "
"now available in Czech, Danish, Greek, Finnish, Indonesian, Macedonian, "
"Norwegian (Bokmål), Russian, Serbian, Swedish, Ukrainian and Vietnamese, "
"and all systemd man pages are now available in German."
msgstr ""
"翻訳者らのおかげで、``man``-page "
"形式でのさらに多くのドキュメントがこれまでより多くの言語で利用できるようになっています。例として、多くの manpage "
"がチェコ語・デンマーク語・ギリシャ語・フィンランド語・インドネシア語・マケドニア語・ノルウェー語 "
"(ブークモール)・ロシア語・セルビア語・スウェーデン語・ウクライナ語・ベトナム語で利用できるようになり、さらに **systemd** の全ての "
"manpage がドイツ語になっています。"

#: ../whats-new.rst:199
#, fuzzy
msgid ""
"To ensure the ``man`` command shows the documentation in your language "
"(where possible), install the right manpages-*lang* package and make sure"
" your locale is correctly configured by using"
msgstr ""
"``man`` コマンドが (可能な場合に) あなたの言語でドキュメントを表示するようにするには、適切な manpages-*lang* "
"パッケージをインストールし、以下のコマンドでロケールを正しく設定してください"

#: ../whats-new.rst:207
msgid "."
msgstr "."

#: ../whats-new.rst:212
msgid "News from Debian Med Blend"
msgstr "Debian Med Blend からのニュース"

#: ../whats-new.rst:214
msgid ""
"As in every release new packages have been added in the fields of "
"medicine and life sciences. The new package **shiny-server** might be "
"worth a particular mention, since it simplifies scientific web "
"applications using ``R``. We also kept up the effort to provide "
"Continuous Integration support for the packages maintained by the Debian "
"Med team."
msgstr ""
"リリースごとに医療・ライフサイエンス分野の新規パッケージが追加されています。新パッケージ **shiny-server** は ``R`` "
"を使った科学向けウェブアプリケーションを簡易に作成してくれるので特に言及する価値があるでしょう。また、我々は Debian Med team "
"がメンテナンスしているパッケージ群へ継続的インテグレーション (CI) サポートの提供に向けた努力を続けています。"

#: ../whats-new.rst:220
msgid ""
"The Debian Med team is always interested in feedback from users, "
"especially in the form of requests for packaging of not-yet-packaged free"
" software, or for backports from new packages or higher versions in "
"testing."
msgstr ""
"Debian Med team "
"は常にユーザーからのフィードバックに興味を持っています。パッケージ化されていないフリーソフトウェアのパッケージ化リクエストや、testing "
"にある新規パッケージやより新しいバージョンのパッケージを backports へ投入することについては特にです。"

#: ../whats-new.rst:225
msgid ""
"To install packages maintained by the Debian Med team, install the "
"metapackages named ``med-*``, which are at version 3.8.x for Debian "
"bookworm. Feel free to visit the `Debian Med tasks pages "
"<https://blends.debian.org/med/tasks>`__ to see the full range of "
"biological and medical software available in Debian."
msgstr ""
"Debian Med team がメンテナンスしているパッケージをインストールするには、``med-*`` という名前の Debian "
"bookworm ではバージョン 3.8.x のメタパッケージをインストールしてください。Debian "
"で入手可能な全範囲の生物・医療関連ソフトウェアを参照するには `Debian Med tasks pages "
"<https://blends.debian.org/med/tasks>`__ にお気軽にお越しください。"

#: ../whats-new.rst:234
msgid "News from Debian Astro Blend"
msgstr "Debian Astro Blend からのニュース"

#: ../whats-new.rst:236
msgid ""
"Debian bookworm comes with version 4.0 of the Debian Astro Pure Blend, "
"which continues to represent a great one-stop solution for professional "
"astronomers, enthusiasts and everyone who is interested in astronomy. "
"Almost all packages in Debian Astro were updated to new versions, but "
"there are also several new software packages."
msgstr ""
"Debian bookworm は Debian Astro Pure Blend バージョン 4.0 として提供されます。Debian "
"Astro Pure Blend "
"は天文学の専門家、天文愛好家、そして天文学に興味を持つ全ての人々へ素晴らしいワンストップソリューションを提供し続けています。Debian "
"Astro のほぼ全てのパッケージは新しいバージョンへ更新されていますが、新しいソフトウェアパッケージもいくつか存在します。"

#: ../whats-new.rst:242
msgid ""
"For radio astronomers, the open source correlator **openvlbi** is now "
"included. The new packages **astap** and **planetary-system-stacker** are"
" useful for image stacking and astrometry resolution. A large number of "
"new drivers and libraries supporting the INDI protocol were packaged and "
"are now shipped with Debian."
msgstr ""
"電波天文学者向けにオープンソースの相関器である **openvlbi** が含まれるようになりました。新規パッケージの **astap** と "
"**planetary-system-stacker** はイメージ・スタッキング法や天体位置測定学に役立ちます。INDI "
"プロトコルをサポートする大量の新しいドライバとライブラリがパッケージ化されて Debian に含まれており、利用できるようになっています。"

#: ../whats-new.rst:248
#, fuzzy
msgid ""
"The new Astropy affiliated packages **python3-extinction**, "
"**python3-sncosmo**, **python3-specreduce**, and **python3-synphot** are "
"included, as well as packages created around **python3-yt** and "
"**python3-sunpy**. Python support for the ASDF file format is much "
"extended, while the Java ecosystem is extended with libraries handling "
"the ECSV and TFCAT file formats, primarily for use with topcat."
msgstr ""
"新規の Astropy 関連パッケージ **python3-extinction**, **python3-sncosmo**, "
"**python3-specreduce** そして **python3-synphot** が含まれており、**python3-yt** および"
" **python3-sunpy** 関連で作成されたパッケージも同様となります。ASDF ファイル形式に対する Python "
"サポートはさらに拡張されていますが、一方でJava エコシステムも主に *topcat* での利用目的で ECSV および TFCAT "
"ファイル形式を取り扱うライブラリによって拡張されています。"

#: ../whats-new.rst:255
msgid ""
"Check `the Astro Blend page <https://blends.debian.org/astro>`__ for a "
"complete list and further information."
msgstr ""
"完全な一覧とさらなる情報を得たい方は `the Astro Blend page "
"<https://blends.debian.org/astro>`__ を確認してください。"

#: ../whats-new.rst:261
msgid "Secure Boot on ARM64"
msgstr "ARM64 でのセキュアブート"

#: ../whats-new.rst:263
msgid ""
"Support for Secure Boot on ARM64 has been reintroduced in |RELEASENAME|. "
"Users of UEFI-capable ARM64 hardware can boot with Secure Boot mode "
"enabled and take full advantage of the security feature. Ensure that the "
"packages **grub-efi-arm64-signed** and **shim-signed** are installed, "
"enable Secure Boot in the firmware interface of your device and reboot to"
" use your system with Secure Boot enabled."
msgstr ""
"ARM64 でのセキュアブートサポートが |RELEASENAME| にて再導入されました。UEFI が利用可能な ARM64 "
"ハードウェアのユーザーはセキュアブートモードを有効にして起動が可能となり、セキュリティ機能の恩恵を最大限に受けることができます。セキュアブートを有効にしたシステムを使うには"
"、**grub-efi-arm64-signed** および **shim-signed** "
"パッケージがインストールされているのを確認してから、デバイスのファームウェア設定画面でセキュアブートを有効にして再起動してください。"

#: ../whats-new.rst:270
msgid ""
"The `Wiki <https://wiki.debian.org/SecureBoot>`__ has more information on"
" how to use and debug Secure Boot."
msgstr ""
"`Wiki <https://wiki.debian.org/SecureBoot>`__ "
"にはセキュアブートの使い方とデバッグ方法についてのさらなる情報があります。"

#: ../whats-new.rst:277
msgid "Something"
msgstr "Something"

#: ../whats-new.rst:279
msgid "Text"
msgstr "Text"

