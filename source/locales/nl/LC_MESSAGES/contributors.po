# Dutch translation of the Debian release notes.
# Copyright (C) 2012 The Debian Project.
# This file is distributed under the same license as the Debian release
# notes.
# Jeroen Schot <schot@a-eskwadraat.nl>, 2012.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2017, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version:  release-notes/release-notes\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2024-01-15 18:03+0100\n"
"PO-Revision-Date: 2019-11-08 17:20+0100\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language: nl\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-"
"dutch@lists.debian.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.3\n"

#: ../contributors.rst:4
msgid "Contributors to the Release Notes"
msgstr "Mensen die een bijdrage hebben geleverd aan de notities bij de release"

#: ../contributors.rst:6
msgid "Many people helped with the release notes, including, but not limited to"
msgstr ""
"Veel mensen hebben geholpen met de notities bij de release waaronder, "
"maar niet alleen,"

#: ../contributors.rst:8
msgid ":abbr:`Adam D. Barrat (various fixes in 2013)`,"
msgstr ":abbr:`Adam D. Barrat (verschillende verbeteringen in 2013)`,"

#: ../contributors.rst:9
msgid ":abbr:`Adam Di Carlo (previous releases)`,"
msgstr ":abbr:`Adam Di Carlo (eerdere uitgaven)`,"

#: ../contributors.rst:10
msgid ":abbr:`Andreas Barth aba (previous releases: 2005 - 2007)`,"
msgstr ":abbr:`Andreas Barth aba (eerdere uitgaven: 2005 - 2007)`,"

#: ../contributors.rst:11
msgid ":abbr:`Andrei Popescu (various contributions)`,"
msgstr ":abbr:`Andrei Popescu (verschillende bijdrages)`,"

#: ../contributors.rst:12
msgid ":abbr:`Anne Bezemer (previous release)`,"
msgstr ":abbr:`Anne Bezemer (eerdere uitgave)`,"

#: ../contributors.rst:13
msgid ":abbr:`Bob Hilliard (previous release)`,"
msgstr ":abbr:`Bob Hilliard (eerdere uitgave)`,"

#: ../contributors.rst:14
msgid ":abbr:`Charles Plessy (description of GM965 issue)`,"
msgstr ":abbr:`Charles Plessy (beschrijving van het probleem met GM965)`,"

#: ../contributors.rst:15
msgid ":abbr:`Christian Perrier bubulle (Lenny installation)`,"
msgstr ":abbr:`Christian Perrier bubulle (de installatie van Lenny)`,"

#: ../contributors.rst:16
msgid ":abbr:`Christoph Berg (PostgreSQL-specific issues)`,"
msgstr ":abbr:`Christoph Berg (PostgreSQL-specifieke aangelegenheden)`,"

#: ../contributors.rst:17
msgid ":abbr:`Daniel Baumann (Debian Live)`,"
msgstr ":abbr:`Daniel Baumann (Debian Live)`,"

#: ../contributors.rst:18
msgid ":abbr:`David Prévot taffit (Wheezy release)`,"
msgstr ":abbr:`David Prévot taffit (de uitgave van Wheezy)`,"

#: ../contributors.rst:19
msgid ":abbr:`Eddy Petrișor (various contributions)`,"
msgstr ":abbr:`Eddy Petrișor (verschillende bijdrages)`,"

#: ../contributors.rst:20
msgid ":abbr:`Emmanuel Kasper (backports)`,"
msgstr ""
":abbr:`Emmanuel Kasper (backports (het geschikt maken van de nieuwste "
"software voor de huidige stabiele release))`,"

#: ../contributors.rst:21
msgid ":abbr:`Esko Arajärvi (rework X11 upgrade)`,"
msgstr ":abbr:`Esko Arajärvi (herwerken van het opwaarderen van X11)`,"

#: ../contributors.rst:22
msgid ":abbr:`Frans Pop fjp (previous release Etch)`,"
msgstr ":abbr:`Frans Pop fjp (eerdere uitgave (Etch))`,"

#: ../contributors.rst:23
msgid ":abbr:`Giovanni Rapagnani (innumerable contributions)`,"
msgstr ":abbr:`Giovanni Rapagnani (talloze bijdragen)`,"

#: ../contributors.rst:24
msgid ":abbr:`Gordon Farquharson (ARM port issues)`,"
msgstr ""
":abbr:`Gordon Farquharson (aandachtspunten bij het geschikt maken van "
"Debian voor ARM)`,"

#: ../contributors.rst:25
msgid ":abbr:`Hideki Yamane henrich (contributed and contributing since 2006)`,"
msgstr ""
":abbr:`Hideki Yamane henrich (heeft bijgedragen en draagt bij sinds "
"2006)`,"

#: ../contributors.rst:26
msgid ":abbr:`Holger Wansing holgerw (contributed and contributing since 2009)`,"
msgstr ""
":abbr:`Holger Wansing holgerw (heeft bijgedragen en draagt bij sinds "
"2009)`,"

#: ../contributors.rst:27
msgid ""
":abbr:`Javier Fernández-Sanguino Peña jfs (Etch release, Squeeze "
"release)`,"
msgstr ""
":abbr:`Javier Fernández-Sanguino Peña jfs (uitgave van Etch, uitgave van "
"Squeeze)`,"

#: ../contributors.rst:28
msgid ":abbr:`Jens Seidel (German translation, innumerable contributions)`,"
msgstr ":abbr:`Jens Seidel (Duitse vertaling, talloze bijdragen)`,"

#: ../contributors.rst:29
msgid ":abbr:`Jonas Meurer (syslog issues)`,"
msgstr ":abbr:`Jonas Meurer (kwesties in verband met syslog)`,"

#: ../contributors.rst:30
msgid ":abbr:`Jonathan Nieder (Squeeze release, Wheezy release)`,"
msgstr ""
":abbr:`Jonathan Nieder (uitgave van Squeeze, uitgave van Wheezy)`,"

#: ../contributors.rst:31
msgid ":abbr:`Joost van Baal-Ilić joostvb (Wheezy release, Jessie release)`,"
msgstr ""
":abbr:`Joost van Baal-Ilić joostvb (uitgave van Wheezy, uitgave van "
"Jessie)`,"

#: ../contributors.rst:32
msgid ":abbr:`Josip Rodin (previous releases)`,"
msgstr ":abbr:`Josip Rodin (eerdere uitgaven)`,"

#: ../contributors.rst:33
msgid ":abbr:`Julien Cristau jcristau (Squeeze release, Wheezy release)`,"
msgstr ":abbr:`Julien Cristau jcristau (uitgave van Squeeze, uitgave van Wheezy)`,"

#: ../contributors.rst:34
msgid ":abbr:`Justin B Rye (English fixes)`,"
msgstr ":abbr:`Justin B Rye (correcties Engels)`,"

#: ../contributors.rst:35
msgid ":abbr:`LaMont Jones (description of NFS issues)`,"
msgstr ":abbr:`LaMont Jones (beschrijving van kwesties in verband met NFS)`,"

#: ../contributors.rst:36
msgid ":abbr:`Luk Claes (editors motivation manager)`,"
msgstr ":abbr:`Luk Claes (motivatiemanager van de redactie)`,"

#: ../contributors.rst:37
msgid ":abbr:`Martin Michlmayr (ARM port issues)`,"
msgstr ""
":abbr:`Martin Michlmayr (aandachtspunten bij het geschikt maken van "
"Debian voor ARM)`,"

#: ../contributors.rst:38
msgid ":abbr:`Michael Biebl (syslog issues)`,"
msgstr ":abbr:`Michael Biebl (kwesties in verband met syslog)`,"

#: ../contributors.rst:39
msgid ":abbr:`Moritz Mühlenhoff (various contributions)`,"
msgstr ":abbr:`Moritz Mühlenhoff (verschillende bijdrages)`,"

#: ../contributors.rst:40
msgid ":abbr:`Niels Thykier nthykier (Jessie release)`,"
msgstr ":abbr:`Niels Thykier nthykier (de uitgave van Jessie)`,"

#: ../contributors.rst:41
msgid ":abbr:`Noah Meyerhans (innumerable contributions)`,"
msgstr ":abbr:`Noah Meyerhans (talloze bijdragen)`,"

#: ../contributors.rst:42
msgid ""
":abbr:`Noritada Kobayashi (Japanese translation (coordination), "
"innumerable contributions)`,"
msgstr ""
":abbr:`Noritada Kobayashi (Japanse vertaling (coördinatie), talloze "
"bijdragen)`,"

#: ../contributors.rst:43
msgid ":abbr:`Osamu Aoki (various contributions)`,"
msgstr ":abbr:`Osamu Aoki (verschillende bijdrages)`,"

#: ../contributors.rst:44
msgid ":abbr:`Paul Gevers elbrus (buster release)`,"
msgstr ":abbr:`Paul Gevers elbrus (de uitgave van Buster)`,"

#: ../contributors.rst:45
msgid ":abbr:`Peter Green (kernel version note)`,"
msgstr ":abbr:`Peter Green (opmerking over kernelversie)`,"

#: ../contributors.rst:46
msgid ":abbr:`Rob Bradford (Etch release)`,"
msgstr ":abbr:`Rob Bradford (de uitgave van Etch)`,"

#: ../contributors.rst:47
msgid ":abbr:`Samuel Thibault (description of d-i Braille support)`,"
msgstr ""
":abbr:`Samuel Thibault (beschrijving van de Braille-ondersteuning in het "
"installatiesysteem van Debian)`,"

#: ../contributors.rst:48
msgid ":abbr:`Simon Bienlein (description of d-i Braille support)`,"
msgstr ""
":abbr:`Simon Bienlein (beschrijving van de Braille-ondersteuning in het "
"installatiesysteem van Debian)`,"

#: ../contributors.rst:49
msgid ":abbr:`Simon Paillard spaillar-guest (innumerable contributions)`,"
msgstr ":abbr:`Simon Paillard spaillar-guest (talloze bijdragen)`,"

#: ../contributors.rst:50
msgid ":abbr:`Stefan Fritsch (description of Apache issues)`,"
msgstr ":abbr:`Stefan Fritsch (beschrijving van kwesties in verband met Apache)`,"

#: ../contributors.rst:51
msgid ":abbr:`Steve Langasek (Etch release)`,"
msgstr ":abbr:`Steve Langasek (de uitgave van Etch)`,"

#: ../contributors.rst:52
msgid ":abbr:`Steve McIntyre (Debian CDs)`,"
msgstr ":abbr:`Steve McIntyre (Debian CDs)`,"

#: ../contributors.rst:53
msgid ":abbr:`Tobias Scherer (description of \"proposed-update\")`,"
msgstr ":abbr:`Tobias Scherer (beschrijving van \"proposed-update\")`,"

#: ../contributors.rst:54
msgid ""
":abbr:`victory victory-guest (markup fixes, contributed and contributing "
"since 2006)`,"
msgstr ""
":abbr:`victory victory-guest (verbeteringen aan de "
"opmaak, heeft bijgedragen en draagt bij sinds 2006)`,"

#: ../contributors.rst:55
msgid ":abbr:`Vincent McIntyre (description of \"proposed-update\")`,"
msgstr ":abbr:`Vincent McIntyre (beschrijving van \"proposed-update\")`,"

#: ../contributors.rst:56
msgid ":abbr:`W. Martin Borgert (editing Lenny release, switch to DocBook XML)`."
msgstr ""
":abbr:`W. Martin Borgert (redactie van de uitgave voor Lenny, "
"omschakeling naar DocBook XML)`."

#: ../contributors.rst:58
msgid ""
"This document has been translated into many languages. Many thanks to all"
" the translators!"
msgstr ""
"Dit document is naar vele talen vertaald. Veel dank aan de vertalers! De "
"volgende personen droegen bij tot de Nederlandse vertaling: :abbr:`Remco "
"Rijnders (2011)`, :abbr:`Eric Spreen (2011)`, :abbr:`Jeroen Schot "
"(2012)`, :abbr:`Vincent Zweije (2012)`, :abbr:`Frans Spiesschaert "
"(2017-2019)`."

