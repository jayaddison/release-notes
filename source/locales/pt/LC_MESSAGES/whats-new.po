# Portuguese translation for Debian's release-notes
# 2006-11-06 - Rui Branco <ruipb@debianpt.org> - initial translation
# Released under the same license as the release notes.
#
# Rui Branco <ruipb@debianpt.org>, 2006-2007.
# Ricardo Silva <ardoric@gmail.com>, 2007.
# Américo Monteiro <a_monteiro@netcabo.pt>, 2009, 2011.
# Miguel Figueiredo <elmig@debianpt.org>, 2007, 2012-2023.
msgid ""
msgstr ""
"Project-Id-Version:  whats-new\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-10-27 12:46+0100\n"
"PO-Revision-Date: 2023-06-11 13:47+0100\n"
"Last-Translator: Miguel Figueiredo <elmig@debianpt.org>\n"
"Language: pt\n"
"Language-Team: Português <traduz@debianpt.org>\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.3\n"

#: ../whats-new.rst:4
msgid "What's new in Debian |RELEASE|"
msgstr "O que há de novo em Debian |RELEASE|"

#: ../whats-new.rst:6
msgid ""
"The `Wiki <https://wiki.debian.org/NewInBookworm>`__ has more information"
" about this topic."
msgstr ""
"O `Wiki <https://wiki.debian.org/NewInBookworm>`__ tem mais informação "
"acerca deste tópico."

#: ../whats-new.rst:12
msgid "Supported architectures"
msgstr "Arquitecturas suportadas"

#: ../whats-new.rst:14
msgid ""
"The following are the officially supported architectures for Debian "
"|RELEASE|:"
msgstr ""
"As seguintes são as arquitecturas suportadas oficialmente em Debian "
"|RELEASE|:"

#: ../whats-new.rst:17
msgid "32-bit PC (``i386``) and 64-bit PC (``amd64``)"
msgstr ""

#: ../whats-new.rst:19
msgid "64-bit ARM (``arm64``)"
msgstr ""

#: ../whats-new.rst:21
msgid "ARM EABI (``armel``)"
msgstr ""

#: ../whats-new.rst:23
msgid "ARMv7 (EABI hard-float ABI, ``armhf``)"
msgstr ""

#: ../whats-new.rst:25
msgid "little-endian MIPS (``mipsel``)"
msgstr ""

#: ../whats-new.rst:27
msgid "64-bit little-endian MIPS (``mips64el``)"
msgstr ""

#: ../whats-new.rst:29
msgid "64-bit little-endian PowerPC (``ppc64el``)"
msgstr ""

#: ../whats-new.rst:31
msgid "IBM System z (``s390x``)"
msgstr ""

#: ../whats-new.rst:36
#, fuzzy
msgid "Baseline bump for 32-bit PC to i686"
msgstr "Aumento da base para 32-bit PC para i686"

#: ../whats-new.rst:34
#, fuzzy
msgid ""
"The 32-bit PC support (known as the Debian architecture i386) now "
"requires the \"long NOP\" instruction. Please refer to "
":ref:`i386-is-i686` for more information."
msgstr ""
"O suporte a 32-bit PC (conhecida como a arquitectura i386 de Debian) "
"agora requer a instrução \"long NOP\". Para mais informação por favor "
"refira-se a :ref:`i386-is-i686`."

#: ../whats-new.rst:38
msgid ""
"You can read more about port status, and port-specific information for "
"your architecture at the `Debian port web pages "
"<https://www.debian.org/ports/>`__."
msgstr ""
"Pode ler mais acerca do estado do port, bem como informação específica do"
" port para a sua arquitectura nas `páginas web dos ports Debian "
"<https://www.debian.org/ports/>`__."

#: ../whats-new.rst:44
msgid "Archive areas"
msgstr "Áreas do arquivo"

#: ../whats-new.rst:46
msgid ""
"The following archive areas, mentioned in the Social Contract and in the "
"Debian Policy, have been around for a long time:"
msgstr ""
"As seguinteas áreas do arquivo, mencionadas no Social Contract e em "
"Debian Policy, existem há muito tempo:"

#: ../whats-new.rst:49
msgid "main: the Debian distribution;"
msgstr "principal: A distribuição Debian"

#: ../whats-new.rst:51
msgid ""
"contrib: supplemental packages intended to work with the Debian "
"distribution, but which require software outside of the distribution to "
"either build or function;"
msgstr ""
"contrib: pacotes suplementares destinados a trabalharem com a "
"distribuição Debian, mas que necessitam de software fora da distribuição "
"ou para compilar ou para funcionar;"

#: ../whats-new.rst:55
msgid ""
"non-free: supplemental packages intended to work with the Debian "
"distribution that do not comply with the DFSG or have other problems that"
" make their distribution problematic."
msgstr ""
"non-free: pacotes suplementares destinados a trabalhar com a distribuição"
" Debian mas que não estão conformes com a DFSG ou têm outros problemas "
"que tornam a sua distribuição problemática."

#: ../whats-new.rst:59
msgid ""
"Following the `2022 General Resolution about non-free firmware "
"<https://www.debian.org/vote/2022/vote_003>`__, the 5th point of the "
"Social Contract was extended with the following sentence:"
msgstr ""
"Seguindo a `2022 Resolução Geral acerca de firmware não-livre "
"<https://www.debian.org/vote/2022/vote_003>`__, o 5º ponto do Social "
"Contract foi estendido com a seguinte frase:"

#: ../whats-new.rst:63
msgid ""
"The Debian official media may include firmware that is otherwise not part"
" of the Debian system to enable use of Debian with hardware that requires"
" such firmware."
msgstr ""
"Os meios oficiais de Debian podem incluir firmware que de outra forma não"
" faz parte do sistema Debian para possibilitar a utilização de Debian com"
" hardware que necessita de tal firmware."

#: ../whats-new.rst:67
msgid ""
"While it's not mentioned explicitly in either the Social Contract or "
"Debian Policy yet, a new archive area was introduced, making it possible "
"to separate non-free firmware from the other non-free packages:"
msgstr ""
"Apesar de ainda não ser mencionado explicitamente no Social Contract ou "
"em Debian Policy, foi introduzido um novo arquivo, tornando assim "
"possível separar o firmware não-livre de outros pacotes non-free:"

#: ../whats-new.rst:71
msgid "non-free-firmware"
msgstr "non-free-firmware"

#: ../whats-new.rst:73
msgid ""
"Most non-free firmware packages have been moved from ``non-free`` to "
"``non-free-firmware`` in preparation for the Debian |RELEASE| release. "
"This clean separation makes it possible to build official installation "
"images with packages from ``main`` and from ``non-free-firmware``, "
"without ``contrib`` or ``non-free``. In turn, these installation images "
"make it possible to install systems with only ``main`` and ``non-free-"
"firmware``, without ``contrib`` or ``non-free``."
msgstr ""
"A maioria dos pacotes de firmware non-free foram movidos de ``non-free`` "
"para ``non-free-firmware`` em preparação para o lançamento Debian "
"|RELEASE|. Esta separação torna possível compilar imagens oficiais de "
"instalação com pacotes de ``main`` e de ``non-free-firmware``, sem "
"``contrib`` ou sem ``non-free``. Por sua vez, estas imagens de instalação"
" tornam possível instalar sistemas com apenas ``main`` e ``non-free-"
"firmware``, sem ``contrib`` e sem ``non-free``."

#: ../whats-new.rst:81
msgid "See :ref:`non-free-firmware` for upgrades from |OLDRELEASENAME|."
msgstr ""
"Veja :ref:`non-free-firmware` para atualizações a partir de "
"|OLDRELEASENAME|."

#: ../whats-new.rst:86
msgid "What's new in the distribution?"
msgstr "O que há de novo na distribuição?"

#: ../whats-new.rst:91
msgid ""
"This new release of Debian again comes with a lot more software than its "
"predecessor |OLDRELEASENAME|; the distribution includes over |PACKAGES-"
"NEW| new packages, for a total of over |PACKAGES-TOTAL| packages. Most of"
" the software in the distribution has been updated: over |PACKAGES-"
"UPDATED| software packages (this is |PACKAGES-UPDATE-PERCENT| of all "
"packages in |OLDRELEASENAME|). Also, a significant number of packages "
"(over |PACKAGES-REMOVED|, |PACKAGES-REMOVED-PERCENT| of the packages in "
"|OLDRELEASENAME|) have for various reasons been removed from the "
"distribution. You will not see any updates for these packages and they "
"will be marked as \"obsolete\" in package management front-ends; see "
":ref:`obsolete`."
msgstr ""
"Este novo lançamento de Debian vem mais uma vez com muito mais software "
"do que o seu predecessor |OLDRELEASENAME|; a distribuição inclui mais de "
"|PACKAGES-NEW| novos pacotes, num total de mais de |PACKAGES-TOTAL| "
"pacotes. A maior parte do software na distribuição foi actualizado: mais "
"de |PACKAGES-UPDATED| pacotes de software (isto é |PACKAGES-UPDATE-"
"PERCENT| de todos os pacotes em |OLDRELEASENAME|). Além disso, um "
"número significativo de pacotes (mais de |PACKAGES-REMOVED|, |PACKAGES-"
"REMOVED-PERCENT| dos pacotes de |OLDRELEASENAME|) foram removidos da "
"distribuição por várias razões. Não verá quaisquer actualizações a estes "
"pacotes e estes serão marcados como \"obsoletos\" nos programas de gestão"
" de pacotes; veja a :ref:`obsolete`."

#: ../whats-new.rst:106
msgid "Desktops and well known packages"
msgstr "Desktops e pacotes muito conhecidos"

#: ../whats-new.rst:108
msgid ""
"Debian again ships with several desktop applications and environments. "
"Among others it now includes the desktop environments GNOME 43, KDE "
"Plasma 5.27, LXDE 11, LXQt 1.2.0, MATE 1.26, and Xfce 4.18."
msgstr ""
"Debian é mais uma vez lançado com vários ambientes de trabalho e "
"aplicações. Entre outros agora inclui os ambientes de trabalho GNOME 43, "
"KDE Plasma 5.27, LXDE 11, LXQt 1.2.0, MATE 1.26, e Xfce 4.18."

#: ../whats-new.rst:112
msgid ""
"Productivity applications have also been upgraded, including the office "
"suites:"
msgstr ""
"As aplicações de produtividade também foram actualizadas, incluindo os "
"conjuntos de ofimática:"

#: ../whats-new.rst:115
msgid "LibreOffice is upgraded to version 7.4;"
msgstr "O LibreOffice foi atualizado para a versão 7.4;"

#: ../whats-new.rst:117
msgid "GNUcash is upgraded to 4.13;"
msgstr "O GNUcash foi atualizado para 4.13;"

#: ../whats-new.rst:119
msgid ""
"Among many others, this release also includes the following software "
"updates:"
msgstr ""
"Entre muitas outras, este lançamento também inclui, as seguintes "
"actualizações de software:"

#: ../whats-new.rst:123
msgid "Package"
msgstr "Pacote"

#: ../whats-new.rst:123
msgid "Version in |OLDRELEASE| (|OLDRELEASENAME|)"
msgstr "Versão em |OLDRELEASE| (|OLDRELEASENAME|)"

#: ../whats-new.rst:123
msgid "Version in |RELEASE| (|RELEASENAME|)"
msgstr "Versão em |RELEASE| (|RELEASENAME|)"

#: ../whats-new.rst:127
msgid "Apache"
msgstr ""

#: ../whats-new.rst:127
msgid "2.4.54"
msgstr "2.4.54"

#: ../whats-new.rst:127 ../whats-new.rst:163
msgid "2.4.57"
msgstr "2.4.57"

#: ../whats-new.rst:129
msgid "Bash"
msgstr ""

#: ../whats-new.rst:129
msgid "5.1"
msgstr "5.1"

#: ../whats-new.rst:129
msgid "5.2.15"
msgstr "5.2.15"

#: ../whats-new.rst:131
msgid "BIND DNS Server"
msgstr ""

#: ../whats-new.rst:131
msgid "9.16"
msgstr "9.16"

#: ../whats-new.rst:131
msgid "9.18"
msgstr "9.18"

#: ../whats-new.rst:133
msgid "Cryptsetup"
msgstr ""

#: ../whats-new.rst:133
msgid "2.3"
msgstr "2.3"

#: ../whats-new.rst:133
msgid "2.6"
msgstr "2.6"

#: ../whats-new.rst:135
msgid "Emacs"
msgstr "Emacs"

#: ../whats-new.rst:135
msgid "27.1"
msgstr "27.1"

#: ../whats-new.rst:135
msgid "28.2"
msgstr "28.2"

#: ../whats-new.rst:137
msgid "Exim default e-mail server"
msgstr ""

#: ../whats-new.rst:137
msgid "4.94"
msgstr "4.94"

#: ../whats-new.rst:137
msgid "4.96"
msgstr "4.96"

#: ../whats-new.rst:140
msgid "GNU Compiler Collection as default compiler"
msgstr ""

#: ../whats-new.rst:140
msgid "10.2"
msgstr "10.2"

#: ../whats-new.rst:140
msgid "12.2"
msgstr "12.2"

#: ../whats-new.rst:144
msgid "GIMP"
msgstr ""

#: ../whats-new.rst:144
msgid "2.10.22"
msgstr "2.10.22"

#: ../whats-new.rst:144
msgid "2.10.34"
msgstr "2.10.34"

#: ../whats-new.rst:146
msgid "GnuPG"
msgstr ""

#: ../whats-new.rst:146
msgid "2.2.27"
msgstr "2.2.27"

#: ../whats-new.rst:146
msgid "2.2.40"
msgstr "2.2.40"

#: ../whats-new.rst:148
msgid "Inkscape"
msgstr ""

#: ../whats-new.rst:148
msgid "1.0.2"
msgstr "1.0.2"

#: ../whats-new.rst:148
msgid "1.2.2"
msgstr "1.2.2"

#: ../whats-new.rst:150
msgid "the GNU C library"
msgstr ""

#: ../whats-new.rst:150
msgid "2.31"
msgstr "2.31"

#: ../whats-new.rst:150
msgid "2.36"
msgstr "2.36"

#: ../whats-new.rst:152
msgid "Linux kernel image"
msgstr "Imagem de kernel Linux"

#: ../whats-new.rst:152
msgid "5.10 series"
msgstr "série 5.10"

#: ../whats-new.rst:152
msgid "6.1 series"
msgstr "série 6.1"

#: ../whats-new.rst:154
msgid "LLVM/Clang toolchain"
msgstr "LLVM/Clang toolchain"

#: ../whats-new.rst:154
msgid "9.0.1 and 11.0.1 (default) and 13.0.1"
msgstr "9.0.1 e 11.0.1 (predefinido) e 13.0.1"

#: ../whats-new.rst:154
msgid "13.0.1 and 14.0 (default) and 15.0.6"
msgstr "13.0.1 e 14.0 (predefinido) e 15.0.6"

#: ../whats-new.rst:157
msgid "MariaDB"
msgstr ""

#: ../whats-new.rst:157
msgid "10.5"
msgstr "10.5"

#: ../whats-new.rst:157
msgid "10.11"
msgstr "10.11"

#: ../whats-new.rst:159
msgid "Nginx"
msgstr ""

#: ../whats-new.rst:159
msgid "1.18"
msgstr "1.18"

#: ../whats-new.rst:159
msgid "1.22"
msgstr "1.22"

#: ../whats-new.rst:161
msgid "OpenJDK"
msgstr ""

#: ../whats-new.rst:161
msgid "11"
msgstr "11"

#: ../whats-new.rst:161
msgid "17"
msgstr "17"

#: ../whats-new.rst:163
msgid "OpenLDAP"
msgstr "OpenLDAP"

#: ../whats-new.rst:163
msgid "2.5.13"
msgstr "2.5.13"

#: ../whats-new.rst:165
msgid "OpenSSH"
msgstr ""

#: ../whats-new.rst:165
msgid "8.4p1"
msgstr "8.4p1"

#: ../whats-new.rst:165
msgid "9.2p1"
msgstr "9,2p1"

#: ../whats-new.rst:167
msgid "OpenSSL"
msgstr ""

#: ../whats-new.rst:167
msgid "1.1.1n"
msgstr "1.1.1n"

#: ../whats-new.rst:167
msgid "3.0.8"
msgstr "3.0.8"

#: ../whats-new.rst:169
msgid "Perl"
msgstr ""

#: ../whats-new.rst:169
msgid "5.32"
msgstr "5.32"

#: ../whats-new.rst:169
msgid "5.36"
msgstr "5.36"

#: ../whats-new.rst:171
msgid "PHP"
msgstr ""

#: ../whats-new.rst:171
msgid "7.4"
msgstr "7.4"

#: ../whats-new.rst:171 ../whats-new.rst:185
msgid "8.2"
msgstr "8.2"

#: ../whats-new.rst:173
msgid "Postfix MTA"
msgstr ""

#: ../whats-new.rst:173
msgid "3.5"
msgstr "3.5"

#: ../whats-new.rst:173
msgid "3.7"
msgstr "3.7"

#: ../whats-new.rst:175
msgid "PostgreSQL"
msgstr ""

#: ../whats-new.rst:175
msgid "13"
msgstr "13"

#: ../whats-new.rst:175
msgid "15"
msgstr "15"

#: ../whats-new.rst:177
msgid "Python 3"
msgstr "Python 3"

#: ../whats-new.rst:177
msgid "3.9.2"
msgstr "3.9.2"

#: ../whats-new.rst:177
msgid "3.11.2"
msgstr "3.11.2"

#: ../whats-new.rst:179
msgid "Rustc"
msgstr "Rustc"

#: ../whats-new.rst:179
msgid "1.48"
msgstr "1.48"

#: ../whats-new.rst:179
msgid "1.63"
msgstr "1.63"

#: ../whats-new.rst:181
msgid "Samba"
msgstr "Samba"

#: ../whats-new.rst:181
msgid "4.13"
msgstr "4.13"

#: ../whats-new.rst:181
msgid "4.17"
msgstr "4.17"

#: ../whats-new.rst:183
msgid "Systemd"
msgstr ""

#: ../whats-new.rst:183
msgid "247"
msgstr "247"

#: ../whats-new.rst:183
msgid "252"
msgstr "252"

#: ../whats-new.rst:185
msgid "Vim"
msgstr "Vim"

#: ../whats-new.rst:185
msgid "9.0"
msgstr "9.0"

#: ../whats-new.rst:191
msgid "More translated man pages"
msgstr "Mais man pages traduzidas"

#: ../whats-new.rst:193
msgid ""
"Thanks to our translators, more documentation in ``man``-page format is "
"available in more languages than ever. For example, many man pages are "
"now available in Czech, Danish, Greek, Finnish, Indonesian, Macedonian, "
"Norwegian (Bokmål), Russian, Serbian, Swedish, Ukrainian and Vietnamese, "
"and all systemd man pages are now available in German."
msgstr ""
"Graças aos nossos tradutores, está disponível mais documentação no "
"formato ``man``-page e em mais linguagens do que nunca. Por exemplo, "
"muitas man pages estão agora disponíveis em Checo, Dinamerquês, Grego, "
"Finlandês, Indonésio, Macedónio, Norueguês (Bokmål), Russo, Sérvio, "
"Sueco, Ucraniano e Vietnamita, e todas as man pages de **systemd** estão "
"agora disponiveis em Alemão."

#: ../whats-new.rst:199
msgid ""
"To ensure the ``man`` command shows the documentation in your language "
"(where possible), install the right manpages-*lang* package and make sure"
" your locale is correctly configured by using"
msgstr ""
"Para assegurar que o comando ``man`` mostra que a documentação na sua "
"linguagem (se possível), instale o pacote certo de manpages-*lang* e "
"assegure-se que o seu locale está configurado corretamente ao usar"

#: ../whats-new.rst:207
msgid "."
msgstr "."

#: ../whats-new.rst:212
msgid "News from Debian Med Blend"
msgstr "Notícias do Blend Debian Med"

#: ../whats-new.rst:214
msgid ""
"As in every release new packages have been added in the fields of "
"medicine and life sciences. The new package **shiny-server** might be "
"worth a particular mention, since it simplifies scientific web "
"applications using ``R``. We also kept up the effort to provide "
"Continuous Integration support for the packages maintained by the Debian "
"Med team."
msgstr ""
"Tal como em todos os lançamentos foram acrescentados novos pacotes nos "
"campos da medicina e das ciências da vida. O novo pacote **shiny-server**"
" pode merecer uma menção especial, já que simplifica as aplicações "
"ciêntificas web que utilizem ``R``. Também mantivemos o esforço para "
"disponibilizar suporte de Integração Contínua para os pacotes mantidos "
"pela equipa Debian Med."

#: ../whats-new.rst:220
msgid ""
"The Debian Med team is always interested in feedback from users, "
"especially in the form of requests for packaging of not-yet-packaged free"
" software, or for backports from new packages or higher versions in "
"testing."
msgstr ""
"A equipa Debian Med está sempre interessada em feedback dos utilizadores,"
" especialmente na forma de pedidos de empacotamento de software livre que"
" ainda não esteja empacotado, ou de backports para novos pacotes ou "
"versões mais maiores em testing."

#: ../whats-new.rst:225
msgid ""
"To install packages maintained by the Debian Med team, install the "
"metapackages named ``med-*``, which are at version 3.8.x for Debian "
"bookworm. Feel free to visit the `Debian Med tasks pages "
"<https://blends.debian.org/med/tasks>`__ to see the full range of "
"biological and medical software available in Debian."
msgstr ""
"Para instalar os pacotes mantidos pela equipa Debian Med, instale os "
"metapacotes com o nome ``med-*``, que estão na versão 3.8.x em Debian "
"bookworm. Sinta-se à vontade para visitar as `páginas das tarefas Debian "
"Med <https://blends.debian.org/med/tasks>`__ para ver toda a gama de "
"software de biologia e de medicina disponível em Debian."

#: ../whats-new.rst:234
msgid "News from Debian Astro Blend"
msgstr "Notícias do Blend Debian Astro"

#: ../whats-new.rst:236
msgid ""
"Debian bookworm comes with version 4.0 of the Debian Astro Pure Blend, "
"which continues to represent a great one-stop solution for professional "
"astronomers, enthusiasts and everyone who is interested in astronomy. "
"Almost all packages in Debian Astro were updated to new versions, but "
"there are also several new software packages."
msgstr ""
"Debian bookwork vem com a versão 4.0 de Debian Astro Pure Blend, que "
"continua a representar uma grande solução para astrónomos profissionais, "
"entusiastas e qualquer interessado em astronomia. Quase todos os pacotes "
"em Debian Astro foram atualizados para novas versões, mas também há "
"alguns novos pacotes de software."

#: ../whats-new.rst:242
msgid ""
"For radio astronomers, the open source correlator **openvlbi** is now "
"included. The new packages **astap** and **planetary-system-stacker** are"
" useful for image stacking and astrometry resolution. A large number of "
"new drivers and libraries supporting the INDI protocol were packaged and "
"are now shipped with Debian."
msgstr ""
"Para os rádio astrónomos, agora é incluido o correlator **openvlbi**. Os "
"novos pacotes **astap** e **planetary-system-stacker** são úteis para "
"empilhamento de imagens e para resolução astronómica. Foram empacotados, "
"e são agora distribuidos com Debian, um grande número de novos drivers e "
"bibliotecas que suportam o protcolo INDI."

#: ../whats-new.rst:248
msgid ""
"The new Astropy affiliated packages **python3-extinction**, "
"**python3-sncosmo**, **python3-specreduce**, and **python3-synphot** are "
"included, as well as packages created around **python3-yt** and "
"**python3-sunpy**. Python support for the ASDF file format is much "
"extended, while the Java ecosystem is extended with libraries handling "
"the ECSV and TFCAT file formats, primarily for use with topcat."
msgstr ""
"Estão incluidos os novos pacotes Astropy afiliados "
"**python3-extinction**, **python3-sncosmo**, **python3-specreduce** e "
"**python3-synphot**, assim como os pacotes **python3-yt** criados e "
"**python3-sunpy**. Foi muito estendido o suporte ao foramto de ficheiro "
"ASDF, enquanto que o ecosistema Java foi estendido com bibliotecas para "
"lidar com os formatos de ficheiros ECSV e TFCAT, primariamente para "
"utilizar com **topcat**."

#: ../whats-new.rst:255
msgid ""
"Check `the Astro Blend page <https://blends.debian.org/astro>`__ for a "
"complete list and further information."
msgstr ""
"Verifique `a página Astro Blend <https://blends.debian.org/astro>`__ para"
" uma lista completa e informação adicional."

#: ../whats-new.rst:261
msgid "Secure Boot on ARM64"
msgstr "Secure Boot em ARM64"

#: ../whats-new.rst:263
msgid ""
"Support for Secure Boot on ARM64 has been reintroduced in |RELEASENAME|. "
"Users of UEFI-capable ARM64 hardware can boot with Secure Boot mode "
"enabled and take full advantage of the security feature. Ensure that the "
"packages **grub-efi-arm64-signed** and **shim-signed** are installed, "
"enable Secure Boot in the firmware interface of your device and reboot to"
" use your system with Secure Boot enabled."
msgstr ""
"O suporte para Secure Boot em ARM64 foi reintroduzido em |RELEASENAME|. "
"Os utilizadores de hardware AMR64 capaz de UEFI podem arrancar com o modo"
" Secure Boot habilitado e tomar toda a vantagem da funcionalidade de "
"segurança. Assegure-se que estão instalados os pacotes **grub-efi-"
"arm64-signed** e **shim-signed**, habilite Secure Boot no interface do "
"firmware do seu dispositivo e reinicie para utilizar o seu sistema com "
"Secure Boot habilitado."

#: ../whats-new.rst:270
msgid ""
"The `Wiki <https://wiki.debian.org/SecureBoot>`__ has more information on"
" how to use and debug Secure Boot."
msgstr ""
"O `Wiki <https://wiki.debian.org/SecureBoot>`__ tem mais informação "
"acerca de como utilizar e fazer debug a Secure Boot."

#: ../whats-new.rst:277
msgid "Something"
msgstr "Alguma coisa"

#: ../whats-new.rst:279
msgid "Text"
msgstr "Texto"

