# German translation of the Debian release notes
#
# Jan Hauke Rahm <info@jhr-online.de>, 2009.
# Holger Wansing <linux@wansing-online.de>, 2010, 2011, 2013, 2015, 2017.
# Holger Wansing <hwansing@mailbox.org>, 2019, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: release-notes 9.0\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2023-11-05 13:18+0100\n"
"PO-Revision-Date: 2021-04-14 23:01+0100\n"
"Last-Translator: Holger Wansing <hwansing@mailbox.org>\n"
"Language: de\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../about.rst:4
msgid "Introduction"
msgstr "Einführung"

#: ../about.rst:6
msgid ""
"This document informs users of the Debian distribution about major "
"changes in version |RELEASE| (codenamed |RELEASENAME|)."
msgstr ""
"Dieses Dokument informiert Benutzer der Debian-Distribution über "
"entscheidende Änderungen in Version |RELEASE| (Codename |RELEASENAME|)."

#: ../about.rst:9
msgid ""
"The release notes provide information on how to upgrade safely from "
"release |OLDRELEASE| (codenamed |OLDRELEASENAME|) to the current release "
"and inform users of known potential issues they could encounter in that "
"process."
msgstr ""
"Die Hinweise zur Veröffentlichung enthalten Informationen, wie ein "
"sicheres Upgrade von Version |OLDRELEASE| (Codename |OLDRELEASENAME|) auf"
" die aktuelle Veröffentlichung durchgeführt werden kann und informieren "
"die Benutzer über bekannte potenzielle Probleme, die während des Upgrades"
" auftreten können."

#: ../about.rst:14
msgid ""
"You can get the most recent version of this document from "
"|URL-R-N-STABLE|."
msgstr "Die neueste Version dieses Dokuments erhalten Sie unter |URL-R-N-STABLE|."

#: ../about.rst:19
msgid ""
"Note that it is impossible to list every known issue and that therefore a"
" selection has been made based on a combination of the expected "
"prevalence and impact of issues."
msgstr ""
"Beachten Sie, dass es unmöglich ist, alle bekannten Probleme aufzulisten;"
" deshalb wurde eine Auswahl getroffen, basierend auf einer Kombination "
"aus der zu erwartenden Häufigkeit des Auftretens und der Auswirkung der "
"Probleme."

#: ../about.rst:23
msgid ""
"Please note that we only support and document upgrading from the previous"
" release of Debian (in this case, the upgrade from |OLDRELEASENAME|). If "
"you need to upgrade from older releases, we suggest you read previous "
"editions of the release notes and upgrade to |OLDRELEASENAME| first."
msgstr ""
"Bitte gestatten Sie uns die Anmerkung, dass wir lediglich ein Upgrade von"
" der letzten Version (in diesem Fall |OLDRELEASENAME|) auf die aktuelle "
"unterstützen können. Falls Sie ein Upgrade von einer noch älteren Version"
" durchführen müssen, empfehlen wir dringend, dass Sie die früheren "
"Ausgaben der Veröffentlichungshinweise lesen und zuerst ein Upgrade auf "
"|OLDRELEASENAME| durchführen."

#: ../about.rst:32
msgid "Reporting bugs on this document"
msgstr "Fehler in diesem Dokument berichten"

#: ../about.rst:34
msgid ""
"We have attempted to test all the different upgrade steps described in "
"this document and to anticipate all the possible issues our users might "
"encounter."
msgstr ""
"Wir haben versucht, die einzelnen Schritte des Upgrades in diesem "
"Dokument zu beschreiben und alle möglicherweise auftretenden Probleme "
"vorherzusehen."

#: ../about.rst:38
msgid ""
"Nevertheless, if you think you have found a bug (incorrect information or"
" information that is missing) in this documentation, please file a bug in"
" the `bug tracking system <https://bugs.debian.org/>`__ against the "
"**release-notes** package. You might first want to review the `existing "
"bug reports <https://bugs.debian.org/release-notes>`__ in case the issue "
"you've found has already been reported. Feel free to add additional "
"information to existing bug reports if you can contribute content for "
"this document."
msgstr ""
"Falls Sie dennoch einen Fehler in diesem Dokument gefunden haben "
"(fehlerhafte oder fehlende Informationen), senden Sie bitte einen "
"entsprechenden Fehlerbericht über das Paket **release-notes** an unsere "
"`Fehlerdatenbank <https://bugs.debian.org/>`__. Sie können auch zunächst "
"die `bereits vorhandenen Fehlerberichte <https://bugs.debian.org/release-"
"notes>`__ lesen für den Fall, dass das Problem, welches Sie gefunden "
"haben, schon berichtet wurde. Sie dürfen gerne zusätzliche Informationen "
"zu solchen bereits vorhandenen Fehlerberichten hinzufügen, wenn Sie "
"Inhalte zu diesem Dokument beitragen können."

#: ../about.rst:47
msgid ""
"We appreciate, and encourage, reports providing patches to the document's"
" sources. You will find more information describing how to obtain the "
"sources of this document in `Sources for this document <#sources>`__."
msgstr ""
"Wir begrüßen Fehlerberichte, die Patches für den Quellcode des Dokuments "
"bereitstellen und möchten Sie sogar dazu ermuntern, solche einzureichen. "
"Mehr Informationen darüber, wie Sie den Quellcode bekommen, finden Sie in"
" `Quelltext dieses Dokuments <#sources>`__."

#: ../about.rst:55
msgid "Contributing upgrade reports"
msgstr "Upgrade-Berichte zur Verfügung stellen"

#: ../about.rst:57
msgid ""
"We welcome any information from users related to upgrades from "
"|OLDRELEASENAME| to |RELEASENAME|. If you are willing to share "
"information please file a bug in the `bug tracking system "
"<https://bugs.debian.org/>`__ against the **upgrade-reports** package "
"with your results. We request that you compress any attachments that are "
"included (using ``gzip``)."
msgstr ""
"Wir begrüßen jede Information von unseren Benutzern, die sich auf ein "
"Upgrade von |OLDRELEASENAME| auf |RELEASENAME| bezieht. Falls Sie solche "
"Informationen bereitstellen möchten, senden Sie bitte einen Fehlerbericht"
" mit den entsprechenden Informationen gegen das Paket **upgrade-reports**"
" an unsere `Fehlerdatenbank <https://bugs.debian.org/>`__. Wir bitten "
"Sie, alle Anhänge, die Sie Ihrem Bericht beifügen, zu komprimieren (mit "
"dem Befehl ``gzip``)."

#: ../about.rst:63
msgid ""
"Please include the following information when submitting your upgrade "
"report:"
msgstr "Bitte fügen Sie Ihrem Upgrade-Bericht folgende Informationen bei:"

#: ../about.rst:66
msgid ""
"The status of your package database before and after the upgrade: "
"**dpkg**'s status database available at ``/var/lib/dpkg/status`` and "
"**apt**'s package state information, available at "
"``/var/lib/apt/extended_states``. You should have made a backup before "
"the upgrade as described at :ref:`data-backup`, but you can also find "
"backups of ``/var/lib/dpkg/status`` in ``/var/backups``."
msgstr ""
"Den Status Ihrer Paketdatenbank vor und nach dem Upgrade: Die "
"Statusdatenbank von **dpkg** finden Sie unter ``/var/lib/dpkg/status``, "
"die Paketstatusinformationen von **apt** unter "
"``/var/lib/apt/extended_states``. Sie sollten vor dem Upgrade eine "
"Sicherung dieser Daten erstellen (wie unter :ref:`data-backup` "
"beschrieben). Sicherungen von ``/var/lib/dpkg/status`` sind aber auch in "
"``/var/backups`` zu finden."

#: ../about.rst:74
msgid ""
"Session logs created using ``script``, as described in :ref:`record-"
"session`."
msgstr ""
"Upgrade-Protokolle, erstellt mit Hilfe des Befehls ``script`` (wie in "
":ref:`record-session` beschrieben)."

#: ../about.rst:77
msgid ""
"Your ``apt`` logs, available at ``/var/log/apt/term.log``, or your "
"``aptitude`` logs, available at ``/var/log/aptitude``."
msgstr ""
"Ihre ``apt``-Logdateien, die Sie unter ``/var/log/apt/term.log`` finden, "
"oder Ihre ``aptitude``-Logdateien unter ``/var/log/aptitude``."

#: ../about.rst:82
msgid ""
"You should take some time to review and remove any sensitive and/or "
"confidential information from the logs before including them in a bug "
"report as the information will be published in a public database."
msgstr ""
"Sie sollten sich ein wenig Zeit nehmen, um die Informationen zu prüfen "
"und sensible bzw. vertrauliche Daten aus den Logdateien zu löschen, bevor"
" Sie die Informationen dem Fehlerbericht anhängen, da der gesamte Bericht"
" mit Ihren Anhängen öffentlich gespeichert und einsehbar sein wird."

#: ../about.rst:89
msgid "Sources for this document"
msgstr "Quelltext dieses Dokuments"

#: ../about.rst:91
msgid ""
"The source of this document is in reStructuredText format, using the "
"sphinx converter. The HTML version is generated using *sphinx-build -b "
"html*. The PDF version is generated using *sphinx-build -b latex*. "
"Sources for the Release Notes are available in the Git repository of the "
"*Debian Documentation Project*. You can use the `web interface "
"<https://salsa.debian.org/ddp-team/release-notes/>`__ to access its files"
" individually through the web and see their changes. For more information"
" on how to access Git please consult the `Debian Documentation Project "
"VCS information pages <https://www.debian.org/doc/vcs>`__."
msgstr ""
"Die Quellen für dieses Dokument liegen im reStructuredText-Format vor, "
"zum Bau wird der sphinx-Konverter verwendet. Die HTML-Version wird mit "
"*sphinx-build -b html* erstellt. Die PDF-Version wird mit *sphinx-build "
"-b latex* erstellt. Die Quellen der Veröffentlichungshinweise sind im "
"GIT-Depot des *Debian Documentation Project* verfügbar. Sie können die "
"`Web-Oberfläche <https://salsa.debian.org/ddp-team/release-notes/>`__ "
"nutzen, um die einzelnen Dateien und ihre Änderungen einzusehen. Für "
"weitere Informationen zum Umgang mit GIT beachten Sie bitte die `GIT-"
"Informationsseiten <https://www.debian.org/doc/vcs>`__ des Debian-"
"Dokumentationsprojekts."

