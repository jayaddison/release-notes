# Danish translation of release-notes for Debian 7.
# Copyright (C) 2015 Free Software Foundation, Inc.
#
# Martin Bagge <brother@bsnet.se>, 2009, 2011.
# Ask Hjorth Larsen <asklarsen@gmail.com>, 2011.
# Joe Hansen <joedalton2@yahoo.dk>, 2012, 2013, 2014, 2015.
msgid ""
msgstr ""
"Project-Id-Version: release-notes 7.0 release-notes.po\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2023-10-06 00:00+0200\n"
"PO-Revision-Date: 2015-04-11 16:20+0100\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language: da\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../index.rst:2
msgid "Release Notes for Debian |RELEASE| (|RELEASENAME|\\)"
msgstr "Udgivelsesnoter til Debian |RELEASE| (|RELEASENAME|\\)"

#: ../index.rst:4
msgid ""
"The Debian Documentation Project `<https://www.debian.org/doc> "
"<https://www.debian.org/doc>`__."
msgstr ""

#: ../index.rst:6
msgid ""
"This document is free software; you can redistribute it and/or modify it "
"under the terms of the GNU General Public License, version 2, as "
"published by the Free Software Foundation."
msgstr ""
"Dette dokument er fri software. Du kan videredistribuere og/eller "
"modificere det under de betingelser, som er angivet i GNU General Public "
"License, version 2, som er udgivet af Free Software Foundation."

#: ../index.rst:10
msgid ""
"This program is distributed in the hope that it will be useful, but "
"WITHOUT ANY WARRANTY; without even the implied warranty of "
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General"
" Public License for more details."
msgstr ""
"Dette dokument distribueres i håb om at det vil vise sig nyttigt, men "
"UDEN NOGEN FORM FOR GARANTI, uden selv de underforståede garantier "
"omkring SALGBARHED eller EGNETHED TIL ET BESTEMT FORMÅL. Yderligere "
"detaljer kan læses i GNU General Public License."

#: ../index.rst:15
msgid ""
"You should have received a copy of the GNU General Public License along "
"with this program; if not, write to the Free Software Foundation, Inc., "
"51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA."
msgstr ""
"Du bør have modtaget en kopi af GNU General Public License sammen med "
"dette dokument. Hvis ikke, så skriv til Free software Foundation, Inc., "
"51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA."

# | msgid ""
# | "The license text can also be found at <ulink url=\"http://www.gnu.org/"
# | "licenses/gpl-2.0.html\"/> and
# <filename>/usr/share/common-licenses/GPL-2</"
# | "filename> on Debian."
#: ../index.rst:19
#, fuzzy
msgid ""
"The license text can also be found at "
"`<https://www.gnu.org/licenses/gpl-2.0.html>`__ and ``/usr/share/common-"
"licenses/GPL-2`` on Debian systems."
msgstr ""
"Licensteksten kan også findes på `<https://www.gnu.org/licenses/gpl-2.0.html>`__ og "
"``/usr/share/common-licenses/GPL-2`` på Debian."
