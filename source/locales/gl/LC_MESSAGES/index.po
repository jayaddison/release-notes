# Translation of release-notes.po to Galician
# Copyright (C) 2021 Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# Pablo <parodper@gmail.com>, 2021.
#
# Traductores:
# Para esta traducción usei (Pablo) o dicionario do Proxecto Trasno
# (http://termos.trasno.gal/) e o DiGaTIC (http://www.digatic.org/gl), nese
# orde de preferencia
msgid ""
msgstr ""
"Project-Id-Version: release-notes 11\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2023-10-06 00:00+0200\n"
"PO-Revision-Date: 2023-06-12 09:34+0200\n"
"Last-Translator: Pablo <parodper@gmail.com>\n"
"Language: gl\n"
"Language-Team: Galician <debian-l10n-galician@lists.debian.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../index.rst:2
msgid "Release Notes for Debian |RELEASE| (|RELEASENAME|\\)"
msgstr "Notas da Versión de Debian |RELEASE| (|RELEASENAME|\\)"

#: ../index.rst:4
msgid ""
"The Debian Documentation Project `<https://www.debian.org/doc> "
"<https://www.debian.org/doc>`__."
msgstr ""

#: ../index.rst:6
msgid ""
"This document is free software; you can redistribute it and/or modify it "
"under the terms of the GNU General Public License, version 2, as "
"published by the Free Software Foundation."
msgstr ""
"Este programa é software libre: vostede pode redistribuílo e/ou "
"modificalo baixo os termos da Licenza pública Xeral de GNU versión 2, "
"publicada pola Free Software Foundation."

#: ../index.rst:10
msgid ""
"This program is distributed in the hope that it will be useful, but "
"WITHOUT ANY WARRANTY; without even the implied warranty of "
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General"
" Public License for more details."
msgstr ""
"Este programa é distribuído coa esperanza de que sexa útil, pero SEN "
"NINGUNHA GARANTÍA; nin sequera a garantía implícita de COMERCIALIDADE ou "
"ADECUACIÓN PARA ALGÚN PROPÓSITO PARTICULAR.  Consulte a Licenza Pública "
"Xeral GNU para máis información."

#: ../index.rst:15
msgid ""
"You should have received a copy of the GNU General Public License along "
"with this program; if not, write to the Free Software Foundation, Inc., "
"51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA."
msgstr ""
"Debería ter recibido unha copia da Licenza pública xeral GNU xunto con "
"este programa; se non fora así, escríballe á Free Software Foundation "
"Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA."

#: ../index.rst:19
msgid ""
"The license text can also be found at "
"`<https://www.gnu.org/licenses/gpl-2.0.html>`__ and ``/usr/share/common-"
"licenses/GPL-2`` on Debian systems."
msgstr ""
"A licenza tamén pode consultarse en `<https://www.gnu.org/licenses/gpl-2.0.html>`__ "
"e ``/usr/share/common-licenses/GPL-2`` nos sistemas Debian."
